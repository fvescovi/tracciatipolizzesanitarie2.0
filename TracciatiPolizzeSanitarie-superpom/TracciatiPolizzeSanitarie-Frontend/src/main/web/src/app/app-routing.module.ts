import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { InviiSuccessiviComponent } from './components/invii-successivi/invii-successivi.component';
import { HomeComponent } from './components/home/home.component';

const routes: Routes = [
  { path: 'home', component: HomeComponent },
  { path: '', component: HomeComponent },
  { path: 'invii-successivi', component: InviiSuccessiviComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
