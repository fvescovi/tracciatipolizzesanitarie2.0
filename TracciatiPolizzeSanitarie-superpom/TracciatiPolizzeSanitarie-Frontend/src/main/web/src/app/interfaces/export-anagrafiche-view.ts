export interface ExportAnagraficheView {
    tipoOperazione: string;
    codiceCompagnia: string;
    codiceConvenzione: string;
    codiceRamo: string;
    agenzia: string;
    dataIngressoAssicurato: string;
    dataUscitaAssicurato: string;
    codiceFiscaleAssicurato: string;
    cognomeAssicurato: string;
    nomeAssicurato: string;
    sessoAssicurato: string;
    dataNascita: string;
    codSezione: string;
    indicatoreCaponucleo: string;
    codiceFiscaleCaponucleo: string;
    indicatoreFisACarico: string;
    numeroPolizza: string;
    dataEffettoPolizza: string;
}
