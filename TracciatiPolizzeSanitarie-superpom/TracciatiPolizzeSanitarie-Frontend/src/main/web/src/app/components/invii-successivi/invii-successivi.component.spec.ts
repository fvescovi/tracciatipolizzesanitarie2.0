import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InviiSuccessiviComponent } from './invii-successivi.component';

describe('InviiSuccessiviComponent', () => {
  let component: InviiSuccessiviComponent;
  let fixture: ComponentFixture<InviiSuccessiviComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InviiSuccessiviComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InviiSuccessiviComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
