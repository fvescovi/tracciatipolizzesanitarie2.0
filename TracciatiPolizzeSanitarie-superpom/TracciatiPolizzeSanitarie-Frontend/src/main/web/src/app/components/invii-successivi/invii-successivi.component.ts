import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatTableDataSource } from '@angular/material';
import { RestJavaService } from 'src/app/services/rest-java.service';
import { ExportAnagraficheView } from '../../interfaces/export-anagrafiche-view';

@Component({
    selector: 'app-invii-successivi',
    templateUrl: './invii-successivi.component.html',
    styleUrls: ['./invii-successivi.component.css'],
})
export class InviiSuccessiviComponent implements OnInit {

    // creazione ogetti
    personaSapList: ExportAnagraficheView[];
    personaSapCessatiList: ExportAnagraficheView[];
    utentiVariatiList: ExportAnagraficheView[];

    // variabili per *ngIf
    respTabella: boolean;
    respMessaggioData: boolean;
    respMessaggioUtenteVuoto: boolean;
    respGif: boolean;

    // variabili messaggi html
    dataInserita: string;
    messaggioData: string;
    messaggioUtenteVuoto: string;
    pathExportExcel: string;

    // variabili mat table material
    displayedColumns1: string[] = ['1', '2', '3', '4', '5'];
    displayedColumns2: string[] = ['1', '2', '3', '4', '5'];
    displayedColumns3: string[] = ['1', '2', '3', '4', '5'];
    dataSource1: MatTableDataSource<ExportAnagraficheView> = null;
    dataSource2: MatTableDataSource<ExportAnagraficheView> = null;
    dataSource3: MatTableDataSource<ExportAnagraficheView> = null;

    constructor(private restJavaService: RestJavaService) {



        this.respTabella = false;
        this.respMessaggioData = false;
        this.respMessaggioUtenteVuoto = false;
        this.respGif = false;


        this.messaggioData = '';
        this.dataInserita = '';
        this.messaggioUtenteVuoto = '';
        this.pathExportExcel = '';


        this.personaSapList = [];
        this.personaSapCessatiList = [];
        this.utentiVariatiList = [];

    }

    ngOnInit() {

    }

    setDataInserita(data: string) {
        if (data === '') {
            this.messaggioData = 'Inserisci una Data!';
            this.respTabella = false;
            this.respMessaggioData = true;
        } else {
            this.respMessaggioData = false;
            this.dataInserita = data;
            // tslint:disable-next-line:max-line-length
            this.pathExportExcel = `${this.restJavaService.uriDatabaseAnagrafiche}rest/getExportSheetExcel/Anagrafiche ${this.dataInserita}.xlsx`;
            this.respGif = true;
            this.stampaExportAnagraficheView(data);
        }
    }

    deleteDateInserita() {
        this.respTabella = false;
        this.dataInserita = '';
        this.respMessaggioUtenteVuoto = false;
        this.respMessaggioData = false;
    }

    stampaExportAnagraficheView(data: string) {
        this.restJavaService.getExportAnagraficheView(data).subscribe((resp) => {
            this.personaSapList = resp[0];
            this.personaSapCessatiList = resp[1];
            this.utentiVariatiList = resp[2];
            this.insertDataInTable();
        });
    }

    insertDataInTable() {
            if (this.personaSapList.length === 0 && this.personaSapCessatiList.length === 0 && this.utentiVariatiList.length === 0) {
               this.respGif = false;
                this.respTabella = false;
                this.messaggioUtenteVuoto = 'Non esistono utenti per la data inserita';
                this.respMessaggioUtenteVuoto = true;
            } else {
                this.dataSource1 = new MatTableDataSource<ExportAnagraficheView>(this.personaSapList);
                this.dataSource2 = new MatTableDataSource<ExportAnagraficheView>(this.personaSapCessatiList);
                this.dataSource3 = new MatTableDataSource<ExportAnagraficheView>(this.utentiVariatiList);
                this.respGif = false;
                this.respTabella = true;
                this.respMessaggioUtenteVuoto = false;
            }
    }

}
