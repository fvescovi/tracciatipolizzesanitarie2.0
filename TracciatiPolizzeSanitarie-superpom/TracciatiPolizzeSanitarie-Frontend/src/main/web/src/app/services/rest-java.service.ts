import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { HttpServiceService } from './http-service.service';
import { ExportAnagraficheView } from '../interfaces/export-anagrafiche-view';
import { browser } from 'protractor';


import { DOCUMENT } from '@angular/platform-browser';


@Injectable({
    providedIn: 'root'
})

export class RestJavaService {

    uriDatabaseAnagrafiche: string;

    constructor(private http: HttpServiceService, @Inject(DOCUMENT) private document) {
        this.uriDatabaseAnagrafiche = document.location.protocol + '//' + document.location.hostname + ':' + document.location.port + '/TracciatiPolizzeSanitarie/';
    }

    getExportAnagraficheView(data: string): Observable<any> {

        return this.http.doGet(`${this.uriDatabaseAnagrafiche}rest/caricaListaAnagrafiche/${data}`);
    }


}
