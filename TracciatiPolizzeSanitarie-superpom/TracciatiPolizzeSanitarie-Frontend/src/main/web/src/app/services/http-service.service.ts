import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class HttpServiceService {

   httpClient: HttpClient;

  constructor(private http: HttpClient) { }

   doGet( url: string  ): Observable<any> {
    return this.http.get<any>(url);
   }

//    doPost(url: string, body: any) {

//     const httpOptions = {
//       headers: new HttpHeaders({
//         // 'Content-Type': contentType, // 'application/json; charset=utf-8',
//         'Access-Control-Allow-Headers': 'content-type',
//         'Access-Control-Allow-Origin': '*',
//         'Allow': ' GET, HEAD, POST, PUT, DELETE, TRACE, OPTIONS, PATCH'
//         // 'Cookie' : this.cookie,
//         // 'X-XSRF-TOKEN': this.token
//       })
//     };

    // return this.http.post<any>( url, body, httpOptions);
//    }

}
