import { TestBed } from '@angular/core/testing';

import { RestJavaService } from './rest-java.service';

describe('RestJavaService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RestJavaService = TestBed.get(RestJavaService);
    expect(service).toBeTruthy();
  });
});
