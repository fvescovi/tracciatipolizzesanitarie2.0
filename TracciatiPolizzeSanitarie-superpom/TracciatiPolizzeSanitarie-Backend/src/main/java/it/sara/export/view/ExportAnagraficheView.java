package it.sara.export.view;

public class ExportAnagraficheView implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	private String tipoOperazione;
	private String codiceCompagnia;
	private String codiceConvenzione;
	private String codiceRamo;
	private String agenzia;
	private String dataIngressoAssicurato;
	private String dataUscitaAssicurato;
	private String codiceFiscaleAssicurato;
	private String cognomeAssicurato;
	private String nomeAssicurato;
	private String sessoAssicurato;
	private String dataNascita;
	private String codSezione;
	private String indicatoreCaponucleo;
	private String codiceFiscaleCaponucleo;
	private String indicatoreFisACarico;
	private String numeroPolizza;
	private String dataEffettoPolizza;

	public ExportAnagraficheView() {
	}

	public ExportAnagraficheView(String tipoOperazione, String codiceCompagnia, String codiceConvenzione,
			String codiceRamo, String agenzia, String dataIngressoAssicurato, String dataUscitaAssicurato,
			String codiceFiscaleAssicurato, String cognomeAssicurato, String nomeAssicurato, String sessoAssicurato,
			String dataNascita, String codSezione, String indicatoreCaponucleo, String codiceFiscaleCaponucleo,
			String indicatoreFisACarico, String numeroPolizza, String dataEffettoPolizza) {
		this.tipoOperazione = tipoOperazione;
		this.codiceCompagnia = codiceCompagnia;
		this.codiceConvenzione = codiceConvenzione;
		this.codiceRamo = codiceRamo;
		this.agenzia = agenzia;
		this.dataIngressoAssicurato = dataIngressoAssicurato;
		this.dataUscitaAssicurato = dataUscitaAssicurato;
		this.codiceFiscaleAssicurato = codiceFiscaleAssicurato;
		this.cognomeAssicurato = cognomeAssicurato;
		this.nomeAssicurato = nomeAssicurato;
		this.sessoAssicurato = sessoAssicurato;
		this.dataNascita = dataNascita;
		this.codSezione = codSezione;
		this.indicatoreCaponucleo = indicatoreCaponucleo;
		this.codiceFiscaleCaponucleo = codiceFiscaleCaponucleo;
		this.indicatoreFisACarico = indicatoreFisACarico;
		this.numeroPolizza = numeroPolizza;
		this.dataEffettoPolizza = dataEffettoPolizza;
	}

	public String getTipoOperazione() {
		return tipoOperazione;
	}

	public void setTipoOperazione(String tipoOperazione) {
		this.tipoOperazione = tipoOperazione;
	}

	public String getCodiceCompagnia() {
		return codiceCompagnia;
	}

	public void setCodiceCompagnia(String codiceCompagnia) {
		this.codiceCompagnia = codiceCompagnia;
	}

	public String getCodiceConvenzione() {
		return codiceConvenzione;
	}

	public void setCodiceConvenzione(String codiceConvenzione) {
		this.codiceConvenzione = codiceConvenzione;
	}

	public String getCodiceRamo() {
		return codiceRamo;
	}

	public void setCodiceRamo(String codiceRamo) {
		this.codiceRamo = codiceRamo;
	}

	public String getAgenzia() {
		return agenzia;
	}

	public void setAgenzia(String agenzia) {
		this.agenzia = agenzia;
	}

	public String getDataIngressoAssicurato() {
		return dataIngressoAssicurato;
	}

	public void setDataIngressoAssicurato(String dataIngressoAssicurato) {
		this.dataIngressoAssicurato = dataIngressoAssicurato;
	}

	public String getDataUscitaAssicurato() {
		return dataUscitaAssicurato;
	}

	public void setDataUscitaAssicurato(String dataUscitaAssicurato) {
		this.dataUscitaAssicurato = dataUscitaAssicurato;
	}

	public String getCodiceFiscaleAssicurato() {
		return codiceFiscaleAssicurato;
	}

	public void setCodiceFiscaleAssicurato(String codiceFiscaleAssicurato) {
		this.codiceFiscaleAssicurato = codiceFiscaleAssicurato;
	}

	public String getCognomeAssicurato() {
		return cognomeAssicurato;
	}

	public void setCognomeAssicurato(String cognomeAssicurato) {
		this.cognomeAssicurato = cognomeAssicurato;
	}

	public String getNomeAssicurato() {
		return nomeAssicurato;
	}

	public void setNomeAssicurato(String nomeAssicurato) {
		this.nomeAssicurato = nomeAssicurato;
	}

	public String getSessoAssicurato() {
		return sessoAssicurato;
	}

	public void setSessoAssicurato(String sessoAssicurato) {
		this.sessoAssicurato = sessoAssicurato;
	}

	public String getDataNascita() {
		return dataNascita;
	}

	public void setDataNascita(String dataNascita) {
		this.dataNascita = dataNascita;
	}

	public String getCodSezione() {
		return codSezione;
	}

	public void setCodSezione(String codSezione) {
		this.codSezione = codSezione;
	}

	public String getIndicatoreCaponucleo() {
		return indicatoreCaponucleo;
	}

	public void setIndicatoreCaponucleo(String indicatoreCaponucleo) {
		this.indicatoreCaponucleo = indicatoreCaponucleo;
	}

	public String getCodiceFiscaleCaponucleo() {
		return codiceFiscaleCaponucleo;
	}

	public void setCodiceFiscaleCaponucleo(String codiceFiscaleCaponucleo) {
		this.codiceFiscaleCaponucleo = codiceFiscaleCaponucleo;
	}

	public String getIndicatoreFisACarico() {
		return indicatoreFisACarico;
	}

	public void setIndicatoreFisACarico(String indicatoreFisACarico) {
		this.indicatoreFisACarico = indicatoreFisACarico;
	}

	public String getNumeroPolizza() {
		return numeroPolizza;
	}

	public void setNumeroPolizza(String numeroPolizza) {
		this.numeroPolizza = numeroPolizza;
	}

	public String getDataEffettoPolizza() {
		return dataEffettoPolizza;
	}

	public void setDataEffettoPolizza(String dataEffettoPolizza) {
		this.dataEffettoPolizza = dataEffettoPolizza;
	}

}
