package it.sara.export.decode;

import org.springframework.context.annotation.Configuration;

@Configuration
public class DecodeCodiceConvenzione extends AbstractTipoDipendente {

	@Override
	public String decode(String tipo) {
		
		super.decode(tipo);
		String codiceConvenzione = null;
		
		switch(tipo) {
		case "I" :
			codiceConvenzione = "41001";
			break;
		case "F":
			codiceConvenzione = "41002";
			break;
		case "D":
			codiceConvenzione = "41003";
			break;
		case "Q":
			codiceConvenzione = "41004";
			break;
		}
		return codiceConvenzione;
	}

}
