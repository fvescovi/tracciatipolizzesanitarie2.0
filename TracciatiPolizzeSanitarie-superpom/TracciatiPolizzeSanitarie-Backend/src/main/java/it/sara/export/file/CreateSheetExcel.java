package it.sara.export.file;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletOutputStream;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.context.annotation.Configuration;

import com.fasterxml.jackson.databind.exc.InvalidFormatException;

import it.sara.export.view.ExportAnagraficheView;

@Configuration
public class CreateSheetExcel {

	public static String[] columns = { "TIPO OPERAZIONE", "CODICE COMPAGNIA", "CODICE CONVENZIONE", "CODICE RAMO",
			"AGENZIA", "DATA INGRESSO ASSICURATO", "DATA USCITA ASSICURATO", "CODICE FISCALE ASSICURATO",
			"COGNOME ASSICURATO", "NOME ASSICURATO", "SESSO ASSICURATO", "DATA NASCITA", "CODICE SEZIONE",
			"INDICATORE CAPONUCLEO", "CODICE FISCALE CAPONUCLEO", "INDICATORE FISCALMENTE A CARICO", "NUMERO POLIZZA",
			"DATA EFFETTO POLIZZA" };

	public static void creazioneFoglioExcel(List<List<ExportAnagraficheView>> listaCompleta, String data,
			ServletOutputStream fileOut) throws IOException, InvalidFormatException {

		Workbook workbook = new XSSFWorkbook();

		Sheet sheet = workbook.createSheet("Anagrafiche");

		Font headerFont = workbook.createFont();
		headerFont.setBold(true);
		headerFont.setFontHeightInPoints((short) 14);
		headerFont.setColor(IndexedColors.BLACK.getIndex());

		CellStyle headerCellStyle = workbook.createCellStyle();
		headerCellStyle.setFont(headerFont);

		Row headerRow = sheet.createRow(0);

		for (int i = 0; i < columns.length; i++) {
			Cell cell = headerRow.createCell(i);
			cell.setCellValue(columns[i]);
			cell.setCellStyle(headerCellStyle);
		}

		int rowNum = 0;

		for (List<ExportAnagraficheView> e : listaCompleta) {

			rowNum++;

			for (ExportAnagraficheView a : e) {

				Row row = sheet.createRow(rowNum++);

				row.createCell(0).setCellValue(a.getTipoOperazione());
				row.createCell(1).setCellValue(a.getCodiceCompagnia());
				row.createCell(2).setCellValue(a.getCodiceConvenzione());
				row.createCell(3).setCellValue(a.getCodiceRamo());
				row.createCell(4).setCellValue(a.getAgenzia());
				row.createCell(5).setCellValue(a.getDataIngressoAssicurato());
				row.createCell(6).setCellValue(a.getDataUscitaAssicurato());
				row.createCell(7).setCellValue(a.getCodiceFiscaleAssicurato());
				row.createCell(8).setCellValue(a.getCognomeAssicurato());
				row.createCell(9).setCellValue(a.getNomeAssicurato());
				row.createCell(10).setCellValue(a.getSessoAssicurato());
				row.createCell(11).setCellValue(a.getDataNascita());
				row.createCell(12).setCellValue(a.getCodSezione());
				row.createCell(13).setCellValue(a.getIndicatoreCaponucleo());
				row.createCell(14).setCellValue(a.getCodiceFiscaleCaponucleo());
				row.createCell(15).setCellValue(a.getIndicatoreFisACarico());
				row.createCell(16).setCellValue(a.getNumeroPolizza());
				row.createCell(17).setCellValue(a.getDataEffettoPolizza());

			}

		}

		for (int i = 0; i < columns.length; i++) {
			sheet.autoSizeColumn(i);
		}

		workbook.write(fileOut);
		workbook.close();

	}

}