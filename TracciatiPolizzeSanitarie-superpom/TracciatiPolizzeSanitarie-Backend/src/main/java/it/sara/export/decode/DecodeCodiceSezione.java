package it.sara.export.decode;

import org.springframework.context.annotation.Configuration;

@Configuration

public class DecodeCodiceSezione extends AbstractTipoDipendente {

	@Override
	public String  decode(String tipo) {
		super.decode(tipo);
		String codiceSezione = null;

		switch (tipo) {
		case "I":
			codiceSezione = "01";
			break;
		case "F":
			codiceSezione = "02";
			break;
		case "D":
			codiceSezione = "03";
			break;
		case "Q":
			codiceSezione = "04";
			break;
		}
		
		return codiceSezione;
	}
	
}
