package it.sara.export.decode;

import org.springframework.context.annotation.Configuration;

@Configuration

public class DecodeNumeroPolizza extends AbstractTipoDipendente {

	@Override
	public String decode(String tipo) {
		super.decode(tipo);
		String numeroPolizza = null;

		switch (tipo) {
		case "I":
			numeroPolizza = "0007324XT";
			break;
		case "F":
			numeroPolizza = "00985189M";
			break;
		case "D":
			numeroPolizza = "00985190M";
			break;
		case "Q":
			numeroPolizza = "00985191M";
			break;
		}
		
		return numeroPolizza;
	}

}
