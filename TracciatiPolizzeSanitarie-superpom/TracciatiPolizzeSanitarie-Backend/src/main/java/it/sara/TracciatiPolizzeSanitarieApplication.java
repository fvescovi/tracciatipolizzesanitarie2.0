package it.sara;

import java.sql.SQLException;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@ComponentScan("it.sara")
@EnableScheduling
public class TracciatiPolizzeSanitarieApplication {

	public static void main(String[] args) throws SQLException {
		
		SpringApplication.run(TracciatiPolizzeSanitarieApplication.class, args);

	}

}