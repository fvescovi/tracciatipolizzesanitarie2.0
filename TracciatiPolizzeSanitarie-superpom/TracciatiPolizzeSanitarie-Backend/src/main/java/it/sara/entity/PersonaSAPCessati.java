package it.sara.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "PERSONA_SAP_CESSATI", schema="ANAGRAFICA")
public class PersonaSAPCessati implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4148424917786029591L;
	
	
	@Id
	@Column(name = "MATRICOLA")
	private String matricola;

	@Column(name = "CODICE_FISCALE")
	private String codiceFiscale;
	
	@Column(name = "DATA_ASSUNZIONE")
	private Date dataAssunzione;

	@Column(name = "MAIL")
	private String mail;

	@Column(name = "COGNOME")
	private String cognome;

	@Column(name = "NOME")
	private String nome;

	@Column(name = "TIPO_DIPENDENTE")
	private String tipoDipendente;


	@Column(name = "DATA_NASCITA")
	private Date dataNascita;

	@Column(name = "SESSO")
	private char sesso;

	@Column(name = "DATA_ELABORAZIONE")
	private Date dataElaborazione;

	@Column(name = "COMUNE_RESIDENZA")
	private String comuneResidenza;

	@Column(name = "CAP_RESIDENZA")
	private String capResidenza;

	@Column(name = "PROVINCIA_RESIDENZA")
	private String provinciaResidenza;

	@Column(name = "PAESE_RESIDENZA")
	private String paeseResidenza;

	@Column(name = "TOPONIMO_RESIDENZA")
	private String toponimoResidenza;

	@Column(name = "INDIRIZZO_RESIDENZA")
	private String indirizzoResidenza;

	@Column(name = "NUMERO_CIVICO_RESIDENZA")
	private String numeroCivicoResidenza;

	@Column(name = "DATA_CESSAZIONE")
	private Date dataCessazione;

	@Column(name = "LUOGO_NASCITA")
	private String luogoNascita;

	@Column(name = "DATA_FINE_POLIZZA")
	private Date dataFinePolizza;

	@Column(name = "DATA_INSERIMENTO")
	private Date dataInserimento;

	@Column(name = "DATA_MODIFICA")
	private Date dataModifica;

	// GET && SET

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getCognome() {
		return cognome;
	}

	public void setCognome(String cognome) {
		this.cognome = cognome;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getTipoDipendente() {
		return tipoDipendente;
	}

	public void setTipoDipendente(String tipoDipendente) {
		this.tipoDipendente = tipoDipendente;
	}

	public String getMatricola() {
		return matricola;
	}

	public void setMatricola(String matricola) {
		this.matricola = matricola;
	}

	public String getCodiceFiscale() {
		return codiceFiscale;
	}

	public void setCodiceFiscale(String codiceFiscale) {
		this.codiceFiscale = codiceFiscale;
	}

	public Date getDataNascita() {
		return dataNascita;
	}

	public void setDataNascita(Date dataNascita) {
		this.dataNascita = dataNascita;
	}

	public char getSesso() {
		return sesso;
	}

	public void setSesso(char sesso) {
		this.sesso = sesso;
	}

	public Date getDataElaborazione() {
		return dataElaborazione;
	}

	public void setDataElaborazione(Date dataElaborazione) {
		this.dataElaborazione = dataElaborazione;
	}

	public String getComuneResidenza() {
		return comuneResidenza;
	}

	public void setComuneResidenza(String comuneResidenza) {
		this.comuneResidenza = comuneResidenza;
	}

	public String getCapResidenza() {
		return capResidenza;
	}

	public void setCapResidenza(String capResidenza) {
		this.capResidenza = capResidenza;
	}

	public String getProvinciaResidenza() {
		return provinciaResidenza;
	}

	public void setProvinciaResidenza(String provinciaResidenza) {
		this.provinciaResidenza = provinciaResidenza;
	}

	public String getPaeseResidenza() {
		return paeseResidenza;
	}

	public void setPaeseResidenza(String paeseResidenza) {
		this.paeseResidenza = paeseResidenza;
	}

	public String getToponimoResidenza() {
		return toponimoResidenza;
	}

	public void setToponimoResidenza(String toponimoResidenza) {
		this.toponimoResidenza = toponimoResidenza;
	}

	public String getIndirizzoResidenza() {
		return indirizzoResidenza;
	}

	public void setIndirizzoResidenza(String indirizzoResidenza) {
		this.indirizzoResidenza = indirizzoResidenza;
	}

	public String getNumeroCivicoResidenza() {
		return numeroCivicoResidenza;
	}

	public void setNumeroCivicoResidenza(String numeroCivicoResidenza) {
		this.numeroCivicoResidenza = numeroCivicoResidenza;
	}

	public Date getDataAssunzione() {
		return dataAssunzione;
	}

	public void setDataAssunzione(Date dataAssunzione) {
		this.dataAssunzione = dataAssunzione;
	}

	public Date getDataCessazione() {
		return dataCessazione;
	}

	public void setDataCessazione(Date dataCessazione) {
		this.dataCessazione = dataCessazione;
	}

	public String getLuogoNascita() {
		return luogoNascita;
	}

	public void setLuogoNascita(String luogoNascita) {
		this.luogoNascita = luogoNascita;
	}

	public Date getDataFinePolizza() {
		return dataFinePolizza;
	}

	public void setDataFinePolizza(Date dataFinePolizza) {
		this.dataFinePolizza = dataFinePolizza;
	}

	public Date getDataInserimento() {
		return dataInserimento;
	}

	public void setDataInserimento(Date dataInserimento) {
		this.dataInserimento = dataInserimento;
	}

	public Date getDataModifica() {
		return dataModifica;
	}

	public void setDataModifica(Date dataModifica) {
		this.dataModifica = dataModifica;
	}

	@Override
	public String toString() {
		return "PersonaSAPCessati [mail=" + mail + ", cognome=" + cognome + ", nome=" + nome + ", tipoDipendente="
				+ tipoDipendente + ", matricola=" + matricola + ", codiceFiscale=" + codiceFiscale + ", dataNascita="
				+ dataNascita + ", sesso=" + sesso + ", dataElaborazione=" + dataElaborazione + ", comuneResidenza="
				+ comuneResidenza + ", capResidenza=" + capResidenza + ", provinciaResidenza=" + provinciaResidenza
				+ ", paeseResidenza=" + paeseResidenza + ", toponimoResidenza=" + toponimoResidenza
				+ ", indirizzoResidenza=" + indirizzoResidenza + ", numeroCivicoResidenza=" + numeroCivicoResidenza
				+ ", dataAssunzione=" + dataAssunzione + ", dataCessazione=" + dataCessazione + ", luogoNascita="
				+ luogoNascita + ", dataFinePolizza=" + dataFinePolizza + ", dataInserimento=" + dataInserimento
				+ ", dataModifica=" + dataModifica + "]";
	}

}
