package it.sara.entity;

import java.io.Serializable;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "PS_ANAGRAFICA_NUCLEO", schema="POLIZZA_SANITARIA")
public class PsAnagraficaNucleo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4784940493650160366L;

	@Id
	@Column(name = "ID_ANAG_NUCLEO")
	private int idAnagNucleo;

	@Column(name = "COD_FISC_CAPONUCLEO")
	private String codFiscCapoNucleo;

	@Column(name = "LIVELLO")
	private String livello;

	@Column(name = "CODICE_FISCALE")
	private String codiceFiscale;

	@Column(name = "COGNOME")
	private String cognome;

	@Column(name = "NOME")
	private String nome;

	@Column(name = "SESSO")
	private char sesso;

	@Column(name = "DATA_NASCITA")
	private Date dataNascita;

	@Column(name = "LUOGO_NASCITA")
	private String luogoNascita;

	@Column(name = "GRADO_PARENTELA")
	private String gradoParentela;

	@Column(name = "FIS_A_CARICO")
	private String fisACarico;

	@Column(name = "DATA_INIZIO_VALIDITA")
	private Date dataInizioValidita;

	@Column(name = "DATA_FINE_VALIDITA")
	private Date dataFineValidita;

	@Column(name = "DATA_MODIFICA")
	private Date dataModifica;

	@Column(name = "DATA_INSERIMENTO")
	private Date dataInserimento;

	@Column(name = "UTENTE_MODIFICA")
	private String utenteModifica;

	@Column(name = "UTENTE_INSERIMENTO")
	private String utenteInserimento;

	@Column(name = "ID_DIPENDENTE")
	private int idDipendente;

	// GET && SET

	public int getIdAnagNucleo() {
		return idAnagNucleo;
	}

	public void setIdAnagNucleo(int idAnagNucleo) {
		this.idAnagNucleo = idAnagNucleo;
	}

	public String getCodFiscCapoNucleo() {
		return codFiscCapoNucleo;
	}

	public void setCodFiscCapoNucleo(String codFiscCapoNucleo) {
		this.codFiscCapoNucleo = codFiscCapoNucleo;
	}

	public String getLivello() {
		return livello;
	}

	public void setLivello(String livello) {
		this.livello = livello;
	}

	public String getCodiceFiscale() {
		return codiceFiscale;
	}

	public void setCodiceFiscale(String codiceFiscale) {
		this.codiceFiscale = codiceFiscale;
	}

	public String getCognome() {
		return cognome;
	}

	public void setCognome(String cognome) {
		this.cognome = cognome;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public char getSesso() {
		return sesso;
	}

	public void setSesso(char sesso) {
		this.sesso = sesso;
	}

	public Date getDataNascita() {
		return dataNascita;
	}

	public void setDataNascita(Date dataNascita) {
		this.dataNascita = dataNascita;
	}

	public String getLuogoNascita() {
		return luogoNascita;
	}

	public void setLuogoNascita(String luogoNascita) {
		this.luogoNascita = luogoNascita;
	}

	public String getGradoParentela() {
		return gradoParentela;
	}

	public void setGradoParentela(String gradoParentela) {
		this.gradoParentela = gradoParentela;
	}

	public String getFisACarico() {
		return fisACarico;
	}

	public void setFisACarico(String fisACarico) {
		this.fisACarico = fisACarico;
	}

	public Date getDataInizioValidita() {
		return dataInizioValidita;
	}

	public void setDataInizioValidita(Date dataInizioValidita) {
		this.dataInizioValidita = dataInizioValidita;
	}

	public Date getDataFineValidita() {
		return dataFineValidita;
	}

	public void setDataFineValidita(Date dataFineValidita) {
		this.dataFineValidita = dataFineValidita;
	}

	public Date getDataModifica() {
		return dataModifica;
	}

	public void setDataModifica(Date dataModifica) {
		this.dataModifica = dataModifica;
	}

	public Date getDataInserimento() {
		return dataInserimento;
	}

	public void setDataInserimento(Date dataInserimento) {
		this.dataInserimento = dataInserimento;
	}

	public String getUtenteModifica() {
		return utenteModifica;
	}

	public void setUtenteModifica(String utenteModifica) {
		this.utenteModifica = utenteModifica;
	}

	public String getUtenteInserimento() {
		return utenteInserimento;
	}

	public void setUtenteInserimento(String utenteInserimento) {
		this.utenteInserimento = utenteInserimento;
	}

	public int getIdDipendente() {
		return idDipendente;
	}

	public void setIdDipendente(int idDipendente) {
		this.idDipendente = idDipendente;
	}

	@Override
	public String toString() {
		return "AnagraficaPSNucleo [idAnagNucleo=" + idAnagNucleo + ", codFiscCapoNucleo=" + codFiscCapoNucleo
				+ ", livello=" + livello + ", codiceFiscale=" + codiceFiscale + ", cognome=" + cognome + ", nome="
				+ nome + ", sesso=" + sesso + ", dataNascita=" + dataNascita + ", luogoNascita=" + luogoNascita
				+ ", gradoParentela=" + gradoParentela + ", fisACarico=" + fisACarico + ", dataInizioValidita="
				+ dataInizioValidita + ", dataFineValidita=" + dataFineValidita + ", dataModifica=" + dataModifica
				+ ", dataInserimento=" + dataInserimento + ", utenteModifica=" + utenteModifica + ", utenteInserimento="
				+ utenteInserimento + ", idDipendente=" + idDipendente + "]";
	}

}