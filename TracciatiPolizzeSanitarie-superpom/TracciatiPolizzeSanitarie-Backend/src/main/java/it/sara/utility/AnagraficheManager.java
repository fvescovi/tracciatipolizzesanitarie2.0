package it.sara.utility;

import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

import it.sara.dao.PersonaSAPCessatiDao;
import it.sara.dao.PersonaSAPDao;
import it.sara.entity.PersonaSAP;
import it.sara.entity.PersonaSAPCessati;
import it.sara.export.view.ExportAnagraficheView;

@Configuration
public class AnagraficheManager {
	
private Logger logger = LoggerFactory.getLogger(AnagraficheManager.class);

    @Autowired
    Transformer transformer;

    @PersistenceContext
    private EntityManager manager;

	@Autowired
	PersonaSAPDao personaSapDao;

	@Autowired
	PersonaSAPCessatiDao personaSapCessatiDao;
	
	
	public List<List<ExportAnagraficheView>> getListComplete(String data) throws SQLException, ParseException {
		
		List<ExportAnagraficheView> listaPersonaSap = getPersonaSap(data);
		List<ExportAnagraficheView> listaPersonaSapCessati = getPersonaSapCessatiList(data);
		List<ExportAnagraficheView> listaPersonaSapVariazioni = getPersonaSapVariazioni(data);
		
		
		List<List<ExportAnagraficheView>> listaCompleta = new ArrayList<List<ExportAnagraficheView>>();
		
		listaCompleta.add(listaPersonaSap);
		listaCompleta.add(listaPersonaSapCessati);
		listaCompleta.add(listaPersonaSapVariazioni);
		
		
		return listaCompleta;
		
	}
	
	

	public List<ExportAnagraficheView> getPersonaSap(String data)
			throws SQLException, ParseException {

		List<PersonaSAP> listaPersonaSap = (List<PersonaSAP>) personaSapDao.findPersonaSapList(data);
		logger.debug("get lista persona sap : " + listaPersonaSap);

		List<ExportAnagraficheView> exportAnagraficheViews = transformer.personaSAPToExportAnagrafiche(listaPersonaSap);

		return exportAnagraficheViews;

	}

	
	public List<ExportAnagraficheView> getPersonaSapCessatiList(String data)
			throws SQLException, ParseException {

		List<PersonaSAPCessati> listaPersonaSapCessati = (List<PersonaSAPCessati>) personaSapCessatiDao
				.findPersonaSapCessatiLista(data);
		logger.debug("get lista persona sap cessati : " + listaPersonaSapCessati);
		
		List<ExportAnagraficheView> exportAnagraficheViews = transformer.personaSAPCessatiToExportAnagrafiche(listaPersonaSapCessati);

		return exportAnagraficheViews;

	}

	
	public List<ExportAnagraficheView> getPersonaSapVariazioni(String data) throws SQLException {
		

		String nativeQuery=" select 'R' tipo_operazione,'0032' codice_compagnia,decode(sap.tipo_dipendente,'I','41001', 'F', '41002', 'D', '41003', 'Q', '41004') codice_convenzione," + 
				"			              '59' codice_ramo," + 
				"			              '8000' agenzia," + 
				"			               to_char(sap.data_assunzione, 'dd-MM-yyyy') data_ingresso_assicurato," + 
				"			               to_char(sap.data_fine_polizza , 'dd-MM-yyyy') data_uscita_assicurato,    " + 
				"			              sap.CODICE_FISCALE codice_fiscale_assicurato,   " + 
				"			              sap.cognome cognome_assicurato,   " + 
				"			              sap.nome nome_assicurato,   " + 
				"			              sap.sesso  sesso_assicurato,   " + 
				"			              to_char(sap.data_nascita, 'dd-MM-yyyy') data_nascita,   " + 
				"			              decode(sap.tipo_dipendente,'I','01', 'F', '02', 'D', '03', 'Q', '04') cod_sezione,   " + 
				"			              'A' indicatore_caponucleo,  " + 
				"			              '' codice_fiscale_caponucleo, " + 
				"			              '' indicatore_fis_a_carico,   " + 
				"			              decode(sap.tipo_dipendente,'I','0007324XT', 'F', '00985189M', 'D', '00985190M', 'Q', '00985191M') numero_polizza,   " + 
				"			              '01012016' data_effetto_polizza   " + 
				"			          FROM anagrafica.persona_sap  sap" + 
				"			              where to_char(DATA_ELABORAZIONE, 'dd/MM/yyyy' ) = '13/02/2019' " + 
				"			                 and sap.CODICE_FISCALE||sap.tipo_dipendente in (select pre.CODICE_FISCALE||pre.tipo_dipendente FROM anagrafica.persona_sap pre" + 
				"			                     where to_char(pre.DATA_ELABORAZIONE, 'yyyy-MM-dd' ) = '"+ data +"')" + 
				"			        minus" + 
				"			 select      'R' tipo_operazione," + 
				"			              '0032' codice_compagnia,decode(sap.tipo_dipendente,'I','41001', 'F', '41002', 'D', '41003', 'Q', '41004') codice_convenzione, " + 
				"			              '59' codice_ramo," + 
				"			              '8000' agenzia," + 
				"			               to_char(sap.data_assunzione, 'dd-MM-yyyy') data_ingresso_assicurato," + 
				"			               to_char(sap.data_fine_polizza , 'dd-MM-yyyy') data_uscita_assicurato,    " + 
				"			              sap.CODICE_FISCALE codice_fiscale_assicurato, " + 
				"			              sap.cognome cognome_assicurato, " + 
				"			              sap.nome nome_assicurato,   " + 
				"			              sap.sesso  sesso_assicurato,   " + 
				"			              ''||to_char(sap.data_nascita, 'dd-MM-yyyy') data_nascita,   " + 
				"			              decode(sap.tipo_dipendente,'I','01', 'F', '02', 'D', '03', 'Q', '04') cod_sezione,   " + 
				"			              'A' indicatore_caponucleo,  " + 
				"			              '' codice_fiscale_caponucleo,  " + 
				"			              '' indicatore_fis_a_carico,   " + 
				"			              decode(sap.tipo_dipendente,'I','0007324XT', 'F', '00985189M', 'D', '00985190M', 'Q', '00985191M') numero_polizza,  " + 
				"			              '01012016' data_effetto_polizza   " + 
				"			          FROM anagrafica.persona_sap  sap" + 
				"			              where to_char(DATA_ELABORAZIONE, 'yyyy-MM-dd' ) = '"+ data +"'" + 
				"			                 and sap.CODICE_FISCALE||sap.tipo_dipendente in (select pre.CODICE_FISCALE||pre.tipo_dipendente FROM anagrafica.persona_sap pre " + 
				"			                     where to_char(pre.DATA_ELABORAZIONE, 'yyyy-MM-dd' ) = '"+ data +"')";
		
		// "+ data +"
		
		List<ExportAnagraficheView> exportAnagraficheViewsList=new ArrayList<ExportAnagraficheView>();
		Query query=manager.createNativeQuery(nativeQuery);
		List queryNativeList=query.getResultList();

		for (Object object : queryNativeList) {
			
			
			Object[] c = (Object[]) object;
			

			ExportAnagraficheView exportAnagraficheView=new ExportAnagraficheView(""+c[0], ""+c[1], ""+c[2],
					""+c[3], ""+c[4],""+c[5],""+c[6],
					""+c[7],""+c[8],""+c[9],""+c[10],
					""+c[11],""+c[12],""+c[13],""+c[14],
					""+c[15],""+c[16],""+c[17]);
			
			exportAnagraficheViewsList.add(exportAnagraficheView);
			
			
		}
		
		return exportAnagraficheViewsList;

	}

}
