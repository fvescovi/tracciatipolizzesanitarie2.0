package it.sara.utility;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

import it.sara.entity.PersonaSAP;
import it.sara.entity.PersonaSAPCessati;
import it.sara.export.decode.DecodeCodiceConvenzione;
import it.sara.export.decode.DecodeCodiceSezione;
import it.sara.export.decode.DecodeNumeroPolizza;
import it.sara.export.view.ExportAnagraficheView;

/**
 * @author a.magnante
 *
 */

@Configuration

public class Transformer {

	@Autowired
	DecodeCodiceConvenzione decodeCodiceConvenzione;

	@Autowired
	DecodeCodiceSezione decodeCodiceSezione;

	@Autowired
	DecodeNumeroPolizza decodeNumeroPolizza;

	public List<ExportAnagraficheView> personaSAPToExportAnagrafiche(List<PersonaSAP> personaSAPList)
			throws ParseException {

		List<ExportAnagraficheView> exportAnagraficheViewsList = new ArrayList<ExportAnagraficheView>();

		for (PersonaSAP personaSAP : personaSAPList) {

			exportAnagraficheViewsList.add(new ExportAnagraficheView("I", "0032",
					decodeCodiceConvenzione.decode(personaSAP.getTipoDipendente()), "59", "8000",
					Utility.formatDatePersonaSap(personaSAP.getDataAssunzione()), "null", personaSAP.getCodiceFiscale(),
					personaSAP.getCognome(), personaSAP.getNome(), "" + personaSAP.getSesso(),
					Utility.formatDatePersonaSap(personaSAP.getDataNascita()),
					decodeCodiceSezione.decode(personaSAP.getTipoDipendente()), "A", "", "",
					decodeNumeroPolizza.decode(personaSAP.getTipoDipendente()),
					Utility.formatDatePersonaSap(new Date("01/01/2016"))));

		}

		return exportAnagraficheViewsList;
	}

	public List<ExportAnagraficheView> personaSAPCessatiToExportAnagrafiche(
			List<PersonaSAPCessati> personaSAPCessatiList) throws ParseException {

		List<ExportAnagraficheView> exportAnagraficheViewsList = new ArrayList<ExportAnagraficheView>();

		for (PersonaSAPCessati personaSAPCessati : personaSAPCessatiList) {

			exportAnagraficheViewsList.add(new ExportAnagraficheView("R", "0032",
					decodeCodiceConvenzione.decode(personaSAPCessati.getTipoDipendente()), "59", "8000",
					Utility.formatDatePersonaSap(personaSAPCessati.getDataAssunzione()),
					Utility.formatDatePersonaSap(personaSAPCessati.getDataFinePolizza() == null ? new Date("00/00/0000")
							: personaSAPCessati.getDataFinePolizza()),
					personaSAPCessati.getCodiceFiscale(), personaSAPCessati.getCognome(), personaSAPCessati.getNome(),
					"" + personaSAPCessati.getSesso(), Utility.formatDatePersonaSap(personaSAPCessati.getDataNascita()),
					decodeCodiceSezione.decode(personaSAPCessati.getTipoDipendente()), "A", "", "",
					decodeNumeroPolizza.decode(personaSAPCessati.getTipoDipendente()),
					Utility.formatDatePersonaSap(new Date("01/01/2016"))));

		}

		return exportAnagraficheViewsList;
	}

}
