package it.sara.utility;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Utility {

	public static String formatDatePersonaSap(Date date) throws ParseException {
		SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.sss'+'ssss");
		SimpleDateFormat outputFormat = new SimpleDateFormat("dd-MM-yyyy");
		String input = inputFormat.format(date);
		Date data = inputFormat.parse(input);
		String formattedDate = outputFormat.format(data);

		return formattedDate;
	}

}
