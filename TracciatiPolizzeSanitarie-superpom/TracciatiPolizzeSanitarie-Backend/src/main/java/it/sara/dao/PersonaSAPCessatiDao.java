package it.sara.dao;

import java.sql.SQLException;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import it.sara.entity.PersonaSAPCessati;

public interface PersonaSAPCessatiDao extends CrudRepository<PersonaSAPCessati, Integer> {

	@Query("select sap FROM PersonaSAPCessati sap where sap.dataFinePolizza between to_date(:data, 'yyyy/MM/dd')+1 and to_date('13/02/2019', 'dd/MM/yyyy')")
	List<PersonaSAPCessati> findPersonaSapCessatiLista(@Param("data") String data) throws SQLException;
	
}