package it.sara.dao;

import java.sql.SQLException;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import it.sara.entity.PersonaSAP;

public interface PersonaSAPDao extends CrudRepository<PersonaSAP, String> {

	@Query("select sap FROM PersonaSAP sap where to_char(sap.dataElaborazione, 'MM/dd/yyyy' ) = '02/13/2019' and sap.codiceFiscale||sap.tipoDipendente not in (select pre.codiceFiscale||pre.tipoDipendente FROM PersonaSAP pre where to_char(pre.dataElaborazione, 'YYYY-MM-DD')=:data)")
	List<PersonaSAP> findPersonaSapList(@Param("data") String data) throws SQLException;

}