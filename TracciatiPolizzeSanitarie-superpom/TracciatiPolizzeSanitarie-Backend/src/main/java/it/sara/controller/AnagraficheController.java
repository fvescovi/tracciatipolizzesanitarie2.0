package it.sara.controller;

import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.exc.InvalidFormatException;

import it.sara.export.file.CreateSheetExcel;
import it.sara.export.view.ExportAnagraficheView;
import it.sara.utility.AnagraficheManager;

@RestController
@RequestMapping("/rest")
@CrossOrigin(origins = "*")
public class AnagraficheController {

	@SuppressWarnings("unused")
	private Logger logger = LoggerFactory.getLogger(AnagraficheController.class);

	@Autowired
	AnagraficheManager anagraficheManager;

	@Autowired
	CreateSheetExcel createSheetExcel;

	// http://localhost:8080/TracciatiPolizzeSanitarie/rest/caricaListaAnagrafiche/2018-12-31

	@RequestMapping(value = "/caricaListaAnagrafiche/{data}", method = RequestMethod.GET, produces = {
			MediaType.APPLICATION_JSON_VALUE })
	public List<List<ExportAnagraficheView>> caricaListaAnagrafiche(@PathVariable(name = "data") String data)
			throws SQLException, ParseException {

		List<List<ExportAnagraficheView>> listaCompleta = anagraficheManager.getListComplete(data);

		return listaCompleta;

	}

	// http://localhost:8080/TracciatiPolizzeSanitarie/rest/getExportSheetExcel/Anagrafiche 2018-12-31.xlsx

	@RequestMapping("/getExportSheetExcel/Anagrafiche {data}.xlsx")
	public void getExportSheetExcel(HttpServletRequest request, HttpServletResponse response,
			@PathVariable("data") String data)
			throws SQLException, ParseException, InvalidFormatException, IOException {

		List<List<ExportAnagraficheView>> listaCompleta = anagraficheManager.getListComplete(data);

		CreateSheetExcel.creazioneFoglioExcel(listaCompleta, data, response.getOutputStream());

	}
}
