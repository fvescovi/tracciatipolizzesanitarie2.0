package it.sara.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class AngularController {

	private Logger logger = LoggerFactory.getLogger(AngularController.class);

	@RequestMapping(value = "/{[path:[^\\.]*}/**")
	public String redirect() {
		logger.info("REDIRECT TO INDEX.HTML");
		return "forward:/index.html";
	}
}

